# Gobbler Implementation

## Installation
This project uses ASP.NET and .NET 6 SDK. 

In order to run this yourself, you'll need to download the .NET SDK from Microsoft https://dotnet.microsoft.com/en-us/download 

## Running
From the root folder, once cloned. You can run dotnet watch for a hot-swappable development environment
    
    dotnet watch

Otherwise use

    dotnet run 


### Notes
This was easy to setup and then difficult to decipher. There are a lot of files that point to a bunch of other places. For a more complex system I'm sure this is more useable and more dynamic but I don't see myself using this in the future for a simple web server.

Followed this tutorial https://dotnet.microsoft.com/en-us/learn/aspnet/hello-world-tutorial/intro and then adjusted based on what I could figure out from reading documentation.